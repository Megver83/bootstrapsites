<?php

// Check for empty fields
if(empty($_POST['fNombre'])  ||
   empty($_POST['fEmail'])  ||
   empty($_POST['fAsunto']) ||
   empty($_POST['fMensaje']) ||
   !filter_var($_POST['fEmail'],FILTER_VALIDATE_EMAIL))
{
    return false;
}

$name = strip_tags(utf8_decode(htmlspecialchars($_POST['fNombre'])));
$subject = strip_tags(utf8_decode(htmlspecialchars($_POST['fAsunto'])));
$email_address = strip_tags(htmlspecialchars($_POST['fEmail']));
$message = strip_tags(utf8_decode(htmlspecialchars($_POST['fMensaje'])));

// Create the email and send the message
$to = 'c@de-chile.cl'; // Add your email address
$email_subject = "[Formulario web de-chile.cl] $subject";
$email_body = "Haz recibido un nuevo mensaje del formulario de contacto de desarrolloeficiente.cl.\n\n"."Aqui estan los detalles:\n\nNombre: $name\n\nEmail: $email_address\n\nMensaje:\n$message";
$headers = "From: noreply@desarrolloeficiente.cl\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
$headers .= "Reply-To: $email_address";
mail($to,$email_subject,$email_body,$headers);
return true;
