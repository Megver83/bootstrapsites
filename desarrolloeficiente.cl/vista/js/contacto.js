$(document).ready(function() {
   // Mostramos el loader
    $().ajaxStart(function() {
        $('#loading').show();
        $('#result').hide();
    }).ajaxStop(function() {
        $('#loading').hide();
        $('#result').fadeIn('slow');
    });
   // Enviamos el formulario
    $('#myform').submit(function() {
   // Definimos el metodo ajax, los datos
        $.ajax({
            type: 'POST',
            //url: $(this).attr('action'),
            url: "./../contacto.php",
            data: $(this).serialize(),
            success: function(data) {
                // Imprimimos la respuesta en el div result
                /*$('#result').html(data);
                $('#result').hide(12000);*/

                $('#fNombre').val("");
                $('#fEmail').val("");
                $('#fAsunto').val("");
                $('#fMensaje').val("");

                mensajeValidacion(data);
                return false;

            }
        })
        return false;
    });
});

function mensajeValidacion(mensaje)
{
    bootbox.dialog({
      message: mensaje,
      title: "Mensaje",
      show: true,
      backdrop: true,
      closeButton: true,
      animate: true,
      className: "my-modal",
      buttons: {
        success: {
          label: "OK",
          className: "btn-success"
                 }
          }
    });
}
