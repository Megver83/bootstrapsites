## Hacking

## SASS to CSS

1. Requeriments:

    ```
    pacman -S sassc
    ```

2. Generate CSS from SASS:

        sassc -t expanded scss/style.scss css/style.css

    or compressed

        sassc -t compressed scss/style.scss css/style.css
